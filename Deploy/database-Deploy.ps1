[CmdletBinding()]
Param
(
	[Parameter(Mandatory=$True,Position=1,HelpMessage="Supply the name of the server to deploy to.")]
	[string]$TargetServer, 
	
	[Parameter(Mandatory=$True, HelpMessage="Supply the name of the database to deploy to. ")]
	[string]$TargetDatabase
)

# Instruct Powershell to not proceed if an error is encountered.
$ErrorActionPreference = 'Stop'

Write-Host '***************************************'
Write-Host 'Deploying to:'$TargetServer $TargetDatabase
Write-Host '***************************************'

#Gets the location of the Script So that it can load the DACPac Functions
$Scriptpath = $MyInvocation.MyCommand.Path | Split-Path

Write-Host $Scriptpath

[string]$SqlPackagePath            = Join-Path ${Env:ProgramFiles(x86)} "Microsoft SQL Server\120\DAC\bin"
[string]$SqlCommandPath            = Join-Path ${Env:ProgramFiles(x86)} "Microsoft SQL Server\90\Tools\Binn"

$env:Path += ";" + $SqlPackagePath
$env:Path += ";" + $SqlCommandPath

# the name of the .dacpac file.
$DACPACFile =".\Northwind.Database.dacpac"
       
$DACPACProfile ="Deploy publish.xml"

$sqlPackageSqlCommand = "SqlPackage.exe /Action:Publish /SourceFile:'{0}' /TargetConnectionString:'Data Source ={1};Integrated Security=SSPI;Initial Catalog={2}' /Profile:'{3}' " -f $DACPACFile, $TargetServer, $TargetDatabase, $DACPACProfile    
    Write-Host $sqlPackageSqlCommand

#now actually execute the publish
Invoke-Expression $sqlPackageSqlCommand