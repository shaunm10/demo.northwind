﻿using System.Collections.Generic;
using System.Web.Http.Results;
using Demo.Northwind.UI.Controllers;
using Moq;
using Northwind.Business;
using Northwind.CommonObject;
using Northwind.TestingUtilities;
using NUnit.Framework;

namespace Northwind.UI.UnitTests.Controllers
{
    [TestFixture(Category ="Products Controller Tests")]
    public class ProductsController_Test
    {
        private ProductsController controllerUnderTest;
        private Mock<IProductService> productServiceMock;


        [SetUp]
        public void SetUp()
        {
            this.productServiceMock = new Mock<IProductService>();
            this.controllerUnderTest = new ProductsController(this.productServiceMock.Object);
        }

        [Test]
        public async void GetProducts_Test()
        {
            // arrange
            var products = new List<Product>();
            var page = RandomDataGenerator.GenerateInt();
            var requestCount = 10;
            var totalCount = RandomDataGenerator.GenerateInt();
            var gridResults = new GridResults<Product>(products, totalCount);
            this.productServiceMock.Setup(x => x.GetProductsAsync(page, requestCount)).ReturnsAsync(gridResults);

            // act
            var result = await this.controllerUnderTest.GetAsync(page);

            // assert
            var okResult = result as OkNegotiatedContentResult<GridResults<Product>>;
            okResult.ShouldNotBeNull();
            okResult.Content.ShouldEqual(gridResults);
        }

        [Test]
        public async void GetProducts_NoPage()
        {
            // arrange:
            int? page = null;

            // act:
            var result = await this.controllerUnderTest.GetAsync(page);

            // assert:
            var badMessageResult = result as BadRequestErrorMessageResult;
            badMessageResult.ShouldNotBeNull();
            badMessageResult.Message.ShouldEqual("page not supplied.");
        }

        [Test]
        public async void PutAsync_HappyPath()
        {
            // arrange:
            var product = new Product();
            var updatedSavedProduct = new Product();
            var id = RandomDataGenerator.GenerateInt(0, 1000);
            this.productServiceMock.Setup(x => x.UpdateProductAsync(id, product)).ReturnsAsync(updatedSavedProduct);

            // act:
            var result = await this.controllerUnderTest.PutAsync(product, id);

            // assert:
            var okResult = result as OkNegotiatedContentResult<Product>;
            okResult.ShouldNotBeNull();
            okResult.Content.ShouldEqual(updatedSavedProduct);
        }

        [Test]
        public async void PutAsync_IdNotSupplied()
        {
            // arrange:
            var product = new Product();
            int? id = null;
            
            // act:
            var result = await this.controllerUnderTest.PutAsync(product, id);

            // assert:
            var badRequestResult = result as BadRequestErrorMessageResult;
            badRequestResult.ShouldNotBeNull();
            badRequestResult.Message.ShouldEqual("id not supplied.");
        }
    }
}
