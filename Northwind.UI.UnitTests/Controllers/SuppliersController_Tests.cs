﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Results;
using Demo.Northwind.UI.Controllers.Api;
using Moq;
using Northwind.Business;
using Northwind.CommonObject;
using NUnit.Framework;

namespace Northwind.UI.UnitTests.Controllers
{
    [TestFixture(Category = "Suppliers Controller Tests")]
    public class SuppliersController_Tests
    {
        private SuppliersController controllerUnderTest;
        private Mock<ISupplierService> supplierServiceMock;

        [SetUp]
        public void SetUp()
        {
            this.supplierServiceMock = new Mock<ISupplierService>();
            this.controllerUnderTest = new SuppliersController(this.supplierServiceMock.Object);
        }

        [Test]
        public async void GetSuppliers_Test()
        {
            // arrange
            var suppliers = new List<Supplier>();
            this.supplierServiceMock.Setup(x => x.GetAllAsync()).ReturnsAsync(suppliers);

            // act
            var result = await this.controllerUnderTest.GetAsync();
            
            // assert 
            var okResult = result as OkNegotiatedContentResult<IEnumerable<Supplier>>;
            okResult.Content.SequenceEqual(suppliers);
        }
    }
}
