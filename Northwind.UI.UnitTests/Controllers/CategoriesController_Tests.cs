﻿using System.Collections.Generic;
using System.Web.Http.Results;
using Demo.Northwind.UI.Controllers.Api;
using Moq;
using Northwind.Business;
using Northwind.CommonObject;
using Northwind.TestingUtilities;
using NUnit.Framework;

namespace Northwind.UI.UnitTests.Controllers
{
    public class CategoriesController_Tests
    {
        private CategoriesController controllerUnderTest;
        private Mock<ICategoryService> categoryServiceMock;

        [SetUp]
        public void SetUp()
        {
            this.categoryServiceMock = new Mock<ICategoryService>();
            this.controllerUnderTest = new CategoriesController(this.categoryServiceMock.Object);
        }

        [Test]
        public async void GetAsync_Test()
        {
            // arrange:
            var categories = new List<Category>();
            this.categoryServiceMock.Setup(x => x.GetAllAsync()).ReturnsAsync(categories);

            // act:
            var result = await this.controllerUnderTest.GetAsync();
            
            // assert:
            var okResult = result as OkNegotiatedContentResult<IEnumerable<Category>>;
            okResult.ShouldNotBeNull();
            okResult.Content.ShouldEqual(categories);
        }
    }
}
