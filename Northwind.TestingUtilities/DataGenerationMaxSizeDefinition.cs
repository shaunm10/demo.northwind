﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Northwind.TestingUtilities
{
    public class DataGenerationMaxSizeDefinition
    {
        public int StringSize = 100;

        public int ByteArraySize = 100;

        public int IntSize = 100;
    }
}
