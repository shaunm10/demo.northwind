﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Northwind.TestingUtilities
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// Performs a quick assert to determine if objects are equal
        /// </summary>
        /// <typeparam name="T">The object that is being extended and compared.</typeparam>
        /// <param name="actualValue">The actual object</param>
        /// <param name="expectedValue">What the object is expected to equal.</param>
        public static void ShouldEqual<T>(this T actualValue, T expectedValue)
        {
            Assert.AreEqual(expectedValue, actualValue);
        }

        /// <summary>
        /// Performs a quick assert to determine if objects are equal
        /// </summary>
        /// <typeparam name="T">The object that is being extended and compared.</typeparam>
        /// <param name="actualValue">The actual object</param>
        /// <param name="expectedValue">What the object is expected to equal.</param>
        /// <param name="errorMessage">The error message to display if this test fails.</param>
        public static void ShouldEqual<T>(this T actualValue, T expectedValue, string errorMessage)
        {
            Assert.AreEqual(expectedValue, actualValue, errorMessage);
        }

        /// <summary>
        /// Performs a quick assert to determine if the object is null
        /// </summary>
        /// <typeparam name="T">The object that is being compared.</typeparam>
        /// <param name="actualValue">The actual object.</param>
        public static void ShouldNotBeNull<T>(this T actualValue)
        {
            Assert.IsNotNull(actualValue);
        }

        /// <summary>
        /// Performs a quick assert to determine if the object is not null
        /// </summary>
        /// <typeparam name="T">The object that is being compared.</typeparam>
        /// <param name="actualValue">The actual object.</param>
        public static void ShouldBeNull<T>(this T actualValue)
        {
            Assert.IsNull(actualValue);
        }

        /// <summary>
        /// Performs a quick assert to determine if objects are the same.
        /// </summary>
        /// <typeparam name="T">The object that is being extended and compared.</typeparam>
        /// <param name="actualValue">The actual object</param>
        /// <param name="expectedValue">What the object is expected to equal.</param>
        public static void IsSameAs<T>(this T actualValue, T expectedValue)
        {
            Assert.AreSame(expectedValue, actualValue);
        }

        /// <summary>
        /// Performs a quick assert to determine if the object is an instance of
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="actualValue"></param>
        /// <param name="expectedType"></param>
        public static void IsAn<T>(this T actualValue, Type expectedType)
        {
            Assert.IsInstanceOf(expectedType, actualValue);
        }

        /// <summary>
        /// Performs reflection on the object and returns the value of a given property.
        /// </summary>
        /// <typeparam name="T">The type being extended</typeparam>
        /// <param name="obj">The instance that is being extended.</param>
        /// <param name="propertyName">The name of the property we are looking for.</param>
        /// <returns>The value of this property.</returns>
        public static object GetReflectedProperty<T>(this T obj, string propertyName)
        {
            PropertyInfo property = obj.GetType().GetProperty(propertyName);

            if (property == null)
            {
                return null;
            }

            return property.GetValue(obj, null);
        }

        /// <summary>
        /// Determine's if an action result redirects to a determined controller/action
        /// </summary>
        /// <param name="actionResult">The action result to integrate.</param>
        /// <param name="expectedRouteValues">The values to use to compare.</param>
        //public static void ShouldBeRedirectionTo(this ActionResult actionResult, object expectedRouteValues)
        //{
        //    var actualValues = ((RedirectToRouteResult)actionResult).RouteValues;
        //    var expectedValues = new RouteValueDictionary(expectedRouteValues);

        //    foreach (string key in expectedValues.Keys)
        //    {
        //        actualValues[key].ShouldEqual(expectedValues[key]);
        //    }
        //}
    }
}
