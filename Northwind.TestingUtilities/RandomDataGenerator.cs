﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Northwind.TestingUtilities
{
    public static class RandomDataGenerator
    {
        /// <summary>
        /// the random class to be used.
        /// </summary>
        private static Random random;

        /// <summary>
        /// Initializes static members of the RandomDataGenerator class.
        /// </summary>
        static RandomDataGenerator()
        {
            random = new Random(Environment.TickCount);
        }

        /// <summary>
        /// Generates a random string of a given size.
        /// </summary>
        /// <param name="length">The length of the string to be generated.</param>
        /// <returns>the random string.</returns>
        public static string GenerateString(int length)
        {
            var result = new StringBuilder();

            for (int i = 0; i < length; i++)
            {
                result.Append(Convert.ToChar(random.Next(65, 90)));
            }

            return result.ToString();
        }

        /// <summary>
        /// Generates a
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static byte[] GenerateByteArray(int length)
        {
            byte[] b = new byte[length];
            random.NextBytes(b);
            return b;
        }

        public static byte GenerateByte()
        {
            return GenerateByteArray(1)[0];
        }

        public static Guid GenerateRandomGuid()
        {
            return Guid.NewGuid();
        }

        /// <summary>
        /// Generates a random integer.
        /// </summary>      
        /// <returns>The random integer</returns>
        public static int GenerateInt()
        {
            return GenerateInt(int.MinValue, int.MaxValue);
        }

        public static short GenerateShort()
        {
            return (short)GenerateInt(short.MinValue, short.MaxValue);
        }

        /// <summary>
        /// Generates a integer, of a given size.
        /// </summary>
        /// <param name="minValue">The minimum size for the integer.</param>
        /// <param name="maxValue">The maximum size for the integer.</param>
        /// <returns>The random integer</returns>
        public static int GenerateInt(int minValue, int maxValue)
        {
            return random.Next(minValue, maxValue);
        }

        public static int GeneratePositiveInt(int? maxValue = null)
        {
            return random.Next(1, maxValue ?? int.MaxValue);
        }

        public static Int16 GenerateInt16()
        {
            return Convert.ToInt16(random.Next(Int16.MinValue, Int16.MaxValue));
        }

        public static long GenerateLong()
        {
            return Convert.ToInt64(random.Next(Int32.MinValue, Int32.MaxValue));
        }

        public static DateTime GenerateDate()
        {
            DateTime start = new DateTime(2009, 1, 1);
            int range = (DateTime.Today - start).Days;
            return start.AddDays(random.Next(range));
        }

        public static DateTimeOffset GenerateDateTimeOffset()
        {
            DateTime start = new DateTime(2009, 1, 1);
            int range = (DateTime.Today - start).Days;
            return new DateTimeOffset(start.AddDays(random.Next(range)));
        }

        public static bool GenerateBoolean()
        {
            return random.NextDouble() > 0.5;
        }

        public static decimal GenerateDecimal()
        {
            var randDouble = random.NextDouble();
            var candidate = decimal.Parse(Math.Round(randDouble / 100, 3).ToString(CultureInfo.InvariantCulture));

            // edge case: we don't ever want to return 0 because that could mean something else.
            if (candidate == decimal.Zero)
            {
                return GenerateDecimal();
            }
            else
            {
                return candidate;
            }
        }

        public static object FillObjectWithData(object objectToBeFilled, DataGenerationMaxSizeDefinition dataGenerationMaxSizeDefinition, bool fillRecursively)
        {
            foreach (PropertyInfo prop in objectToBeFilled.GetType().GetProperties())
            {
                FillProperty(objectToBeFilled, dataGenerationMaxSizeDefinition, fillRecursively, prop);
            }

            return objectToBeFilled;
        }

        private static void FillProperty(object objectToBeFilled, DataGenerationMaxSizeDefinition dataGenerationMaxSizeDefinition, bool fillRecursively, PropertyInfo prop)
        {
            if (prop.CanWrite)
            {
                if (prop.PropertyType == typeof(string))
                {
                    prop.SetValue(objectToBeFilled, RandomDataGenerator.GenerateString(dataGenerationMaxSizeDefinition.StringSize), null);
                }
                else if (prop.PropertyType == typeof(int) || prop.PropertyType == typeof(int?))
                {
                    prop.SetValue(objectToBeFilled, RandomDataGenerator.GenerateInt(0, dataGenerationMaxSizeDefinition.IntSize), null);
                }
                else if (prop.PropertyType == typeof(Int16) || prop.PropertyType == typeof(Int16?))
                {
                    prop.SetValue(objectToBeFilled, RandomDataGenerator.GenerateInt16(), null);
                }
                else if (prop.PropertyType == typeof(decimal) || prop.PropertyType == typeof(decimal?))
                {
                    prop.SetValue(objectToBeFilled, GenerateDecimal(), null);
                }
                else if (prop.PropertyType == typeof(DateTime) || prop.PropertyType == typeof(DateTime?))
                {
                    prop.SetValue(objectToBeFilled, RandomDataGenerator.GenerateDate(), null);
                }
                else if (prop.PropertyType == typeof(bool) || prop.PropertyType == typeof(bool?))
                {
                    prop.SetValue(objectToBeFilled, RandomDataGenerator.GenerateBoolean(), null);
                }
                else if (prop.PropertyType == typeof(byte[]))
                {
                    prop.SetValue(objectToBeFilled, RandomDataGenerator.GenerateByteArray(dataGenerationMaxSizeDefinition.ByteArraySize));
                }
                else if (prop.PropertyType.GetInterfaces().ToList().Contains(typeof(System.Collections.IEnumerable)))
                {
                    // is if this is a generic type.
                    if (prop.PropertyType.IsGenericType)
                    {
                        // if so does it have multiple generic arguments?
                        if (prop.PropertyType.GetGenericArguments().Count() > 1)
                        {
                            Assert.Fail(
                                "This unit test is only implemented to handle 1 generic argument. type {0}, contains {1}",
                                prop.PropertyType.FullName,
                                prop.PropertyType.GetGenericArguments().Count());
                        }

                        // get the Generic argument
                        var genericType = prop.PropertyType.GetGenericArguments()[0];
                        var genericTypeInstanciated = Activator.CreateInstance(genericType);

                        if (IsCustomClass(genericType))
                        {
                            // now recursively fill this object.
                            FillObjectWithData(genericTypeInstanciated, dataGenerationMaxSizeDefinition, false);
                        }
                        else
                        {
                            Assert.Fail(
                                "This test is not setup to fill lists of primitive types, as need for {0}.{1}",
                                objectToBeFilled.GetType().FullName,
                                prop.Name);
                        }

                        var genericListType = typeof(List<>).MakeGenericType(genericType);

                        var listInstance = Activator.CreateInstance(genericListType);
                        var addMethod = genericListType.GetMethod("Add");

                        if (addMethod == null)
                        {
                            Assert.Fail("No Add method found on {0}", genericListType.FullName);
                        }

                        // finally add a item to it.
                        addMethod.Invoke(listInstance, new object[1] { genericTypeInstanciated });

                        // assign the property to the newly created list.
                        prop.SetValue(objectToBeFilled, listInstance, null);
                    }
                    else
                    {
                        Assert.Fail(
                            "This unit test is only implemented to handle generic arguments in Lists. type {0}, does not contain a generic type.",
                            prop.PropertyType.FullName);
                    }
                }
                else if (prop.PropertyType.IsEnum)
                {
                    var enumType = prop.PropertyType;
                    var enumerationValues = enumType.GetMembers(BindingFlags.Public | BindingFlags.Static);
                    var pickedRandomInt = RandomDataGenerator.GenerateInt(0, enumerationValues.Count());

                    prop.SetValue(objectToBeFilled, Enum.ToObject(enumType, pickedRandomInt), null);
                }
                else if (IsCustomClass(prop.PropertyType))
                {
                    if (fillRecursively)
                    {
                        var instanciatedProperty = Activator.CreateInstance(prop.PropertyType);
                        prop.SetValue(
                            objectToBeFilled,
                            FillObjectWithData(instanciatedProperty, dataGenerationMaxSizeDefinition, true),
                            null);
                    }
                }
                else
                {
                    Assert.Fail(
                        "Property '{0}' on class '{1}' has not been handled and cannot be filled with random data. Please update test to handle type {2}",
                        prop.Name,
                        objectToBeFilled.GetType().FullName,
                        prop.PropertyType);
                }
            }
        }

        /// <summary>
        /// Determine's if the type is a custom class.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal static bool IsCustomClass(Type type)
        {
            if (type.IsPrimitive
                || type == typeof(decimal) || type == typeof(decimal?)
                || type == typeof(string)
                || type == typeof(DateTime) || type == typeof(DateTime?)
                || type.IsEnum)
            {
                return false;
            }

            return true;
        }
    }
}
