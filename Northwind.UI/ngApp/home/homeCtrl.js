﻿(function () {
    'use strict';

    angular
        .module('northWind')
        .controller('homeController', homeController);

    homeController.$inject = ['$location'];

    function homeController($location) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'homeCtrl';

        activate();

        function activate() {

        }
    }
})();
