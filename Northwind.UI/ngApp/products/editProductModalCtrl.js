﻿(function () {
    'use strict';

    angular
        .module('northWind')
        .controller('editProductModalCtrl', editProductModalCtrl);

    editProductModalCtrl.$inject = ['$scope', '$q', '$uibModalInstance', 'product', 'suppliers', 'categories', 'productsSvc', 'errorSvc', 'userNotificationSvc'];

    function editProductModalCtrl($scope, $q, $uibModalInstance, product, suppliers, categories, productsSvc, errorSvc, userNotificationSvc) {
        
        var vm = this;
        vm.product = product;
        vm.suppliers = suppliers;
        vm.categories = categories;

        vm.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        vm.save = function () {

            // save to the http endpoint.
            var savePromise = productsSvc.updateProduct(vm.product.productId, vm.product);

            vm.showSpinner = true;
            $q.all({
                save: savePromise
            }).then(function(response) {

                userNotificationSvc.displaySuccess('Product Saved!', 'Success');

                // tell the parent we are done.
                $uibModalInstance.close(vm.product);
            }, errorSvc.ajaxError).finally(function() {
                vm.showSpinner = false;
            });
            
        };

        activate();

        function activate() {
            
        }
    }
})();
