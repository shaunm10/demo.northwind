﻿(function () {
    'use strict';

    angular
        .module('northWind')
        .controller('productsController', productsController);

    productsController.$inject = ['$location', '$q', '$uibModal', 'categorySvc', 'productsSvc', 'supplierSvc', 'errorSvc'];

    function productsController($location, $q, $uibModal, categorySvc, productsSvc, supplierSvc, errorSvc) {

        var vm = this;

        vm.pageChanged = function () {
            getData();  
        };

        vm.showEditProduct = function (product) {
            var modalInstance = $uibModal.open(
            {
                animation: true,
                templateUrl: 'editProductModal.html',
                controller: 'editProductModalCtrl',
                controllerAs: 'vm',
                resolve: {
                    product: function() {return angular.copy(product)},
                    suppliers: function() {return vm.suppliers},
                    categories: function() {return vm.categories}
                    
                }
            }
            );

            modalInstance.result.then(function () {

                // save as successful
                
                // update the grid.
                getData();

            }, function () {
                // cancel
            });
        };

        function getData() {

            vm.showSpinner = true;

            var productsPromise = productsSvc.getProducts(vm.currentPage);

            $q.all({
                products: productsPromise
            }).then(function (response) {
                vm.products = response.products.data.data;
                vm.totalItems = response.products.data.totalCount;
            }, errorSvc.ajaxError)
            .finally(function () {
                vm.showSpinner = false;
            });

        }

        function activate() {

            vm.showSpinner = true;

            var categoryPromise = categorySvc.getCategories();
            var supplierPromise = supplierSvc.getSuppliers();

            $q.all({
                categories: categoryPromise,
                suppliers: supplierPromise
            }).then(function (response) {
                vm.suppliers = response.suppliers.data;
                vm.categories = response.categories.data;

                // default to the first page in the pager.
                vm.currentPage = 1;

                getData();

            }, errorSvc.ajaxError).finally(function () {
                vm.showSpinner = false;
            });


        }

        activate();

    }
})();
