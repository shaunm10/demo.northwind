﻿(function () {
    'use strict';

    angular
        .module('northWind')
        .factory('productsSvc', productsSvc);

    productsSvc.$inject = ['$http'];

    function productsSvc($http) {

        var getProducts = function(page) {
            return $http({
                method: 'GET',
                url: '/api/Products',
                params: {
                    page: page
                }
            });
        };

        var updateProduct = function(id, product) {
            return $http({
                method: 'PUT',
                url: '/api/Products/' + id,
                data: product
            });
        };

        return {
            getProducts: getProducts,
            updateProduct: updateProduct
        }
    }
})();