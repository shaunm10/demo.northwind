﻿(function () {
    var app = angular.module('northWind', ['ui.router','ui.bootstrap']);

    // configure the router.
    app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');

        $stateProvider.state("home",
        {
            url: "/",
            templateUrl: "/ngView/home",
            controller: 'homeController'

        }).state("products",
        {
            url: "/products",
            templateUrl: "/ngView/products",
            controller: 'productsController',
            controllerAs: 'vm'
        });
    }]);


    // add 3rd party lib's
    app.factory('toastr', function () {
        return window.toastr;
    });

})();