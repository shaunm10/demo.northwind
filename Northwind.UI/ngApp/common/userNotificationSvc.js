﻿(function () {
    'use strict';

    angular
        .module('northWind')
        .factory('userNotificationSvc', userNotificationSvc);

    userNotificationSvc.$inject = ['toastr'];

    function userNotificationSvc(toastr) {
        var defaultOptions = {
            timeOut: 1000
        };

        var displayWarning = function (message, title, options) {
            toastr.options = angular.extend({}, defaultOptions, options);

            toastr.warning(message, title);
        };

        var displaySuccess = function (message, title, options) {
            toastr.options = angular.extend({}, defaultOptions, options, { closeButton: true });
            toastr.success(message, title);
        };

        var displayError = function (message, title, options) {
            toastr.options = angular.extend({}, defaultOptions, options);
            toastr.error(message, title);
        };

        var clearNotifications = function () {
            toastr.clear();
        };

        return {
            displayWarning: displayWarning,
            displaySuccess: displaySuccess,
            displayError: displayError,
            clearNotifications: clearNotifications
        };
    }
})();