﻿(function () {
    'use strict';

    angular
        .module('northWind')
        .factory('categorySvc', categorySvc);

    categorySvc.$inject = ['$http'];

    function categorySvc($http) {

        var getCategories = function () {
            return $http({
                method: 'GET',
                url: '/api/Categories'
            });
        }

        return {
            getCategories: getCategories
        }
    }
})();