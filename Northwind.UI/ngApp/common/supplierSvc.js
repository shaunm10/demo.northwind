﻿(function () {
    'use strict';

    angular
        .module('northWind')
        .factory('supplierSvc', supplierSvc);

    supplierSvc.$inject = ['$http'];

    function supplierSvc($http) {

        var getSuppliers = function () {
            return $http({
                method: 'GET',
                url: '/api/Suppliers'
            });
        }

        return {
            getSuppliers: getSuppliers
        }
    }
})();