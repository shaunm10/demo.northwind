﻿(function () {
    'use strict';

    angular
        .module('northWind')
        .factory('errorSvc', errorSvc);

    errorSvc.$inject = ['$http', 'toastr'];

    function errorSvc($http, toastr) {
        var service = {
            ajaxError: ajaxError
        };

        return service;

        function ajaxError(error) {
            // we don't want this to throw additional
            // errors, so wrapping it in try catch.
            try {

                var title = 'Unable to retrieve data.';
                var reason = '';

                if (error.statusText) {

                    reason = error.statusText;

                    // try to get more information.
                    if (error.data && error.data.exceptionMessage) {
                        reason += ' : ' + error.data.exceptionMessage;
                    } else if (error.data && error.data.message) {
                        reason += ' : ' + error.data.message;

                        // if this object has modelState, see what's inside.
                        if (error.data.modelState) {
                            var modelState = error.data.modelState;

                            reason += '<ul>';
                            // iterate through all the properties in the modelState
                            for (var prop in modelState) {

                                // verify this property belongs to this object,
                                // not prototyped in.
                                if (modelState.hasOwnProperty(prop)) {

                                    // a property can have multiple errors, 
                                    // iterate through these
                                    for (var n = 0; modelState[prop].length > n; n++) {
                                        if (modelState[prop][n]) {
                                            reason += '<li>' + modelState[prop][n] + '</li>';
                                        }
                                    }
                                }
                            }
                            reason += '</ul>';
                        }
                    } else if (error.config && error.config.url) {
                        reason += ' : ' + error.config.url;
                    }
                }

                // reset the toastr options.
                toastr.options = {};
                toastr.options.closeButton = true;

                toastr.error(reason, title);
            }
            catch (innerError) {
                // squash the exception.
            }
        }
    }
})();