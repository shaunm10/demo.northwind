﻿using System.Configuration;

namespace Demo.Northwind.UI.Infrustructor
{
    public static class Configuration
    {
        public static string  HeaderColor
        {
            get { return ConfigurationManager.AppSettings["HeaderColor"]; }
        }

        public static string SubHeader
        {
            get { return ConfigurationManager.AppSettings["SubHeader"]; }
        }
    }
}