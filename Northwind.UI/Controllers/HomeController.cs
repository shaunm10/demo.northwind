﻿using System.Web.Mvc;
using Northwind.Business;

namespace Demo.Northwind.UI.Controllers
{
    public class HomeController : Controller
    {
        private readonly IProductService productService;
         
        public HomeController(IProductService productService)
        {
            this.productService = productService;
        }
        public ActionResult Index()
        {
            var products = this.productService.GetProductsAsync(10);   
            return View(products);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}