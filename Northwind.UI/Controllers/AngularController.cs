﻿
using System.Web.Mvc;

namespace Demo.Northwind.UI.Controllers
{
    public class AngularController : Controller
    {
        // GET: Angular
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NGView(string viewName)
        {
            return this.View(viewName);
        }
    }
}