﻿using System.Threading.Tasks;
using System.Web.Http;
using Northwind.Business;
using Northwind.CommonObject;

namespace Demo.Northwind.UI.Controllers
{
    public class ProductsController : ApiController
    {
        private readonly IProductService productService;

        public ProductsController(IProductService productService)
        {
            this.productService = productService;
        }

        public async Task<IHttpActionResult> GetAsync([FromUri]int? page = null)
        {
            if (!page.HasValue)
            {
                return this.BadRequest($"{nameof(page)} not supplied.");
            }

            var products = await this.productService.GetProductsAsync(page.Value, 10);
            return this.Ok(products);
        }

        public async Task<IHttpActionResult> PutAsync([FromBody]Product product, int? id = null)
        {
            if (!id.HasValue)
            {  
               return this.BadRequest($"{nameof(id)} not supplied.");
            }

            var savedProduct = await this.productService.UpdateProductAsync(id.Value, product);
           
            return this.Ok(savedProduct);
        }
    }
}
