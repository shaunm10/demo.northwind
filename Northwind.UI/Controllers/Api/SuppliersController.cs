﻿using System.Threading.Tasks;
using System.Web.Http;
using Northwind.Business;

namespace Demo.Northwind.UI.Controllers.Api
{
    public class SuppliersController : ApiController
    {
        private readonly ISupplierService supplierService;

        public SuppliersController(ISupplierService supplierService)
        {
            this.supplierService = supplierService;
        }

        // GET: Suppliers
        public async Task<IHttpActionResult> GetAsync()
        {
            var suppliers = await this.supplierService.GetAllAsync();
            return this.Ok(suppliers);
        }
    }
}
