﻿using System.Threading.Tasks;
using System.Web.Http;
using Northwind.Business;
using NUnit.Framework;

namespace Demo.Northwind.UI.Controllers.Api
{
    [TestFixture(Category = "Categories Controller Tests")]
    public class CategoriesController : ApiController
    {
        private readonly ICategoryService categoryService;

        public CategoriesController(ICategoryService categoryService)
        {
            this.categoryService = categoryService;
        }

        // GET: Categories
        public async Task<IHttpActionResult> GetAsync()
        {
            var categories = await this.categoryService.GetAllAsync();
            return this.Ok(categories);
        }
    }
}