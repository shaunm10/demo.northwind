﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Demo.Northwind.UI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "AngularView",
               url: "ngView/{viewName}",
               defaults: new { Controller = "Angular", action = "NGView" });


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Angular", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
