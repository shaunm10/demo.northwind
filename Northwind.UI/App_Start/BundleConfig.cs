﻿using System.Web;
using System.Web.Optimization;

namespace Demo.Northwind.UI
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            // this includes all OUR JavaScript files.
            bundles.Add(new ScriptBundle("~/bundles/App").IncludeDirectory("~/ngApp/", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/Libs").Include(
                "~/Scripts/toastr.js",
                "~/Scripts/jquery-1.10.2.min.js",
                "~/Scripts/angular.min.js",
                "~/Scripts/angular-ui-router.min.js",
                "~/Scripts/angular-animate.min.js",
                "~/Scripts/angular-ui/ui-bootstrap.min.js",
                "~/Scripts/angular-ui/ui-bootstrap-tpls.min.js"));
        }
    }
}
