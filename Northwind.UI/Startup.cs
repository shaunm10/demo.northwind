﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Demo.Northwind.UI.Startup))]
namespace Demo.Northwind.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
