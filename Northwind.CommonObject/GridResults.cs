﻿
using System.Collections.Generic;

namespace Northwind.CommonObject
{
    public class GridResults<T>
    {
        public GridResults(IEnumerable<T> data, int totalCount)
        {
            this.Data = data;
            this.TotalCount = totalCount;
        }

        public IEnumerable<T> Data { get; }

        public int TotalCount { get;}
    }
}
