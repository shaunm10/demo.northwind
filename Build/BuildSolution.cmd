@ECHO OFF
REM A convenience script to avoid having to type the full path of MSBuild
@ECHO ON
"C:/Program Files (x86)/MSBuild/14.0/Bin/amd64/msbuild.exe" RestoreNugetAndBuild.proj /p:VisualStudioVersion=14.0

pause;