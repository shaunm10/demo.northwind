﻿DECLARE @Region TABLE
(
    [RegionID] [int] NOT NULL,
	[RegionDescription] [nchar](50) NOT NULL
);
 
SET NOCOUNT ON
 
INSERT INTO @Region
VALUES
       (1, 'Eastern'),
       (2, 'Western'),
       (3, 'Northern'),
       (4, 'Southern')
 
MERGE dbo.Region AS Target
USING (SELECT [RegionID], [RegionDescription] FROM @Region) AS Source
ON (Target.[RegionID] = Source.[RegionID])
       WHEN MATCHED THEN
              UPDATE SET
                     [RegionDescription] = Source.[RegionDescription]
       WHEN NOT MATCHED BY TARGET THEN
              INSERT([RegionID], [RegionDescription])
              VALUES(Source.[RegionID], Source.[RegionDescription])
       WHEN NOT MATCHED BY SOURCE THEN
              DELETE;
             
SET NOCOUNT Off