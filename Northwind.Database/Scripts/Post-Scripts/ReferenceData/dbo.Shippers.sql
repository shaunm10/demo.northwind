﻿DECLARE @Shippers TABLE
(
    [ShipperID] [int] NOT NULL,
	[CompanyName] [nvarchar](40) NOT NULL,
	[Phone] [nvarchar](24) NULL

);
 
SET NOCOUNT ON
 
INSERT INTO @Shippers
VALUES
       (1, 'Speedy Express','(503) 555-9831'),
       (2, 'United Package', '(503) 555-3199'),
       (3, 'Federal Shipping', '(503) 555-9931')
	   
 
MERGE dbo.Shippers AS Target
USING (SELECT [ShipperID], [CompanyName], [Phone]  FROM @Shippers) AS Source
ON (Target.[ShipperID] = Source.[ShipperID])
       WHEN MATCHED THEN
              UPDATE SET
                     [CompanyName] = Source.[CompanyName],
                     [Phone] = Source.[Phone]

       WHEN NOT MATCHED BY TARGET THEN
              INSERT([ShipperID], [CompanyName], [Phone])
              VALUES(Source.[ShipperID], Source.[CompanyName], Source.[Phone])
       WHEN NOT MATCHED BY SOURCE THEN
              DELETE;
             
SET NOCOUNT Off