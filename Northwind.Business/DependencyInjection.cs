﻿using Ninject;
using Ninject.Extensions.Conventions;

namespace Northwind.Business
{
    public class DependencyInjection
    {
        public static void Configure(IKernel kernel)
        {
            // kernel.Bind<IProductService>().To<ProductService>();

            kernel.Bind(x =>
            {
                x.FromThisAssembly()
                    .SelectAllClasses()
                    .BindDefaultInterface();
            });

            Persistance.DependencyInjection.Configure(kernel);
        }
    }
}
