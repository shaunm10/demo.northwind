﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Northwind.CommonObject;

namespace Northwind.Business
{
    public interface ISupplierService
    {
        Task<IEnumerable<Supplier>> GetAllAsync();
    }
}
