﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Northwind.CommonObject;
using Northwind.Persistance;

namespace Northwind.Business
{
    public class SupplierService : ISupplierService
    {
        private readonly ISupplierRepository supplierRepository;

        public SupplierService(ISupplierRepository supplierRepository)
        {
            this.supplierRepository = supplierRepository;
        }

        public async Task<IEnumerable<Supplier>> GetAllAsync()
        {
            var suppliers = await this.supplierRepository.GetAllAsync();
            return suppliers; 
        }
    }
}
