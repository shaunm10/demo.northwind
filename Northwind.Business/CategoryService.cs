﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Northwind.CommonObject;
using Northwind.Persistance;

namespace Northwind.Business
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository categoryRepository;

        public CategoryService(ICategoryRepository categoryRepository)
        {
            this.categoryRepository = categoryRepository;
        }

        public async Task<IEnumerable<Category>> GetAllAsync()
        {
            var categories = await this.categoryRepository.GetAllAsync();
            return categories;
        }
    }
}
