﻿using System.Threading.Tasks;
using Northwind.CommonObject;

namespace Northwind.Business
{
    public interface IProductService
    {
        Task<GridResults<Product>> GetProductsAsync(int? page = 1, int? requestCount = 10);

        Task<Product> UpdateProductAsync(int productId, Product product);
    }
}
