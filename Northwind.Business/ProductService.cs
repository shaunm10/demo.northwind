﻿using System.Threading.Tasks;
using Northwind.CommonObject;
using Northwind.Persistance;

namespace Northwind.Business
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository productRepository;

        public ProductService(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }

        public async Task<GridResults<Product>> GetProductsAsync(int? page = 1, int? requestCount = 10)
        {
            var products = await this.productRepository
                .GetProducts(page.Value, requestCount.Value);
            return products;
        }

        public async Task<Product> UpdateProductAsync(int productId, Product product)
        {
            var originalProduct = await productRepository.GetProductById(productId);

            // 'simple' update methodology
            originalProduct.CategoryId = product.CategoryId;
            originalProduct.Discontinued = product.Discontinued;
            originalProduct.ProductName = product.ProductName;
            originalProduct.QuantityPerUnit = product.QuantityPerUnit;
            originalProduct.ReorderLevel = product.ReorderLevel;
            originalProduct.SupplierId = product.SupplierId;
            originalProduct.UnitPrice = product.UnitPrice;

            var savedProduct = await this.productRepository.Save(originalProduct);
            return savedProduct;
        }
    }
}
