﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Northwind.CommonObject;

namespace Northwind.Persistance
{
   
    public class CategoryRepository : ICategoryRepository
    {

        private readonly NorthwindDataContext databaseContext;

        public CategoryRepository(NorthwindDataContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        public async Task<IEnumerable<Category>> GetAllAsync()
        {
            var categories = await databaseContext.Categories.ToListAsync();

            return categories;
        }
    }
}
