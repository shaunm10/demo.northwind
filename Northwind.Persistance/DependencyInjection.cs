﻿using System.Configuration;
using Ninject;
using Ninject.Extensions.Conventions;
using Ninject.Web.Common;

namespace Northwind.Persistance
{
    public static class DependencyInjection
    {
        public static void Configure(IKernel kernel)
        {   
            var connectionString = ConfigurationManager.ConnectionStrings["Northwind"].ConnectionString;

            //kernel.Bind<NorthwindDataContext>().ToConstant(new NorthwindDataContext(connectionString));

            kernel.Bind<NorthwindDataContext>().ToSelf().InRequestScope();
            
            // auto register classes in this assembly.
            kernel.Bind(x =>
            {
                x.FromThisAssembly()
                    .SelectAllClasses()
                    .BindDefaultInterface();
            });
        }
    }
}
