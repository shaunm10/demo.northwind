﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Northwind.CommonObject;

namespace Northwind.Persistance
{
    public class ProductRepository : IProductRepository
    {
        private readonly NorthwindDataContext databaseContext;

        public ProductRepository(NorthwindDataContext dbContext)
        {
            this.databaseContext = dbContext;
        }

        public async Task<Product> GetProductById(int productId)
        {
            var products = await this.databaseContext.Products
                .Where(x => x.ProductId == productId)
                .ToListAsync();

            return products.FirstOrDefault();
        }

        public async Task<GridResults<Product>> GetProducts(int pageNumber, int requestCount)
        {
            var numberOfProductsToSkip = (pageNumber * requestCount) - requestCount;

            var productCount = await this.databaseContext.Products
                .CountAsync();

            var products = await this.databaseContext.Products
                .Include(x=> x.Supplier)
                .Include(x=> x.Category)
                .OrderBy(x=> x.ProductId)
                .Skip(numberOfProductsToSkip)
                .Take(requestCount)
                .ToListAsync();

            var gridResults = new GridResults<Product>(products, productCount);

            return gridResults;
        }

        public async Task<Product> Save(Product product)
        {
            this.databaseContext.Entry(product).State = EntityState.Modified;
            var result = await this.databaseContext.SaveChangesAsync();
            return product;
        }
    }
}
