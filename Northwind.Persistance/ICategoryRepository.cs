﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Northwind.CommonObject;

namespace Northwind.Persistance
{
    public interface ICategoryRepository
    {
        Task<IEnumerable<Category>> GetAllAsync();
    }
}
