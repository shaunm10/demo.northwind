﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Northwind.CommonObject;

namespace Northwind.Persistance
{
    public class SupplierRepository : ISupplierRepository
    {
        private readonly NorthwindDataContext databaseContext;

        public SupplierRepository(NorthwindDataContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        public async Task<IEnumerable<Supplier>> GetAllAsync()
        {
            var suppliers = await this.databaseContext.Suppliers.ToListAsync();
            return suppliers;
        }
    }
}
