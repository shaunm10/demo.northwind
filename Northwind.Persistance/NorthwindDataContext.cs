﻿using System.Data.Entity;
using Northwind.CommonObject;

namespace Northwind.Persistance
{
    public class NorthwindDataContext : DbContext
    {
        private readonly string connectionString;

        public NorthwindDataContext(string connectionString) : base(connectionString)
        {
            this.connectionString = connectionString;
            // turns off the migrations
            Database.SetInitializer<NorthwindDataContext>(null);
        }

        public NorthwindDataContext()
        {
            this.connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Northwind"].ConnectionString;
            this.Database.Connection.ConnectionString = connectionString;

        }

        public DbSet<Product> Products { get; set; }

        public DbSet<Supplier> Suppliers { get; set; }

        public DbSet<Category> Categories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().ToTable("Products");
            modelBuilder.Entity<Product>().HasKey(x => x.ProductId);

            // https://msdn.microsoft.com/en-us/data/jj591620.aspx#Introduction
            modelBuilder.Entity<Supplier>().ToTable("Suppliers");
            modelBuilder.Entity<Supplier>().HasKey(x => x.SupplierId);
            
            modelBuilder.Entity<Category>().ToTable("Categories");
            modelBuilder.Entity<Category>().HasKey(x => x.CategoryId);

            base.OnModelCreating(modelBuilder);
        }

        
    }
}
