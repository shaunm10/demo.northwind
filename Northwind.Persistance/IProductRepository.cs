﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Northwind.CommonObject;

namespace Northwind.Persistance
{
    public interface IProductRepository
    {
        Task<GridResults<Product>> GetProducts(int pageNumber, int requestCount);

        Task<Product> GetProductById(int productId);

        Task<Product> Save(Product product);

    }
}
