﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Moq;
using Northwind.CommonObject;
using Northwind.Persistance;
using Northwind.TestingUtilities;
using NUnit.Framework;

namespace Northwind.Business.UnitTests
{
    [TestFixture(Category = "ProductServiceTests")]
    public class ProductService_Tests
    {
        private Mock<IProductRepository> productRepoMock;
        private ProductService serviceUnderTest;

        [SetUp]
        public void SetUp()
        {
            this.productRepoMock = new Mock<IProductRepository>();
            this.serviceUnderTest = new ProductService(this.productRepoMock.Object);
        }

        [Test]
        public async void GetProducts_Test()
        {
            // arrange
            var requestCount = 10;
            var page = RandomDataGenerator.GenerateInt(1, 10);
            var totalCount = RandomDataGenerator.GenerateInt();
            var products = new List<Product>();
            var gridResults = new GridResults<Product>(products, totalCount);
            this.productRepoMock.Setup(x => x.GetProducts(page, requestCount)).ReturnsAsync(gridResults);

            // act
            var result = await serviceUnderTest.GetProductsAsync(page);

            // assert
            result.ShouldEqual(gridResults);
        }

        [Test]
        public async void UpdateProductAsync_Test()
        {
            // arrange:
            var productId = RandomDataGenerator.GenerateInt(1, 1000);
            var newUpdatedProduct = new Product
            {
                ProductId = productId,
                CategoryId = RandomDataGenerator.GenerateInt(),
                Discontinued = RandomDataGenerator.GenerateBoolean(),
                ProductName = RandomDataGenerator.GenerateString(100),
                QuantityPerUnit = RandomDataGenerator.GenerateString(20),
                ReorderLevel = RandomDataGenerator.GenerateShort(),
                SupplierId = RandomDataGenerator.GenerateInt(),
                UnitPrice = RandomDataGenerator.GenerateDecimal()
            };

            var originalProduct = new Product
            {
                ProductId = newUpdatedProduct.ProductId
            };

            var justSavedProduct = new Product();

            // mock the call to get the original product
            this.productRepoMock.Setup(x => x.GetProductById(productId)).ReturnsAsync(originalProduct);
            
            // mock the call to save the product.
            this.productRepoMock.Setup(x => x.Save(It.Is<Product>(m =>
                m.ProductId == newUpdatedProduct.ProductId &&
                m.CategoryId == newUpdatedProduct.CategoryId &&
                m.ProductName == newUpdatedProduct.ProductName &&
                m.QuantityPerUnit == newUpdatedProduct.QuantityPerUnit &&
                m.ReorderLevel == newUpdatedProduct.ReorderLevel &&
                m.SupplierId == newUpdatedProduct.SupplierId &&
                m.UnitPrice == newUpdatedProduct.UnitPrice
                ))).ReturnsAsync(justSavedProduct);

            // act:
            var result = await serviceUnderTest.UpdateProductAsync(productId, newUpdatedProduct);

            // assert:
            result.ShouldEqual(justSavedProduct);
        }
    }
}
