﻿using System.Collections.Generic;
using Moq;
using Northwind.CommonObject;
using Northwind.Persistance;
using Northwind.TestingUtilities;
using NUnit.Framework;

namespace Northwind.Business.UnitTests
{
    [TestFixture(Category = "CategoryServiceTests")]
    public class CategoryService_Tests
    {
        private Mock<ICategoryRepository> categoryRepoMock;
        private CategoryService serviceUnderTest;
        
        [SetUp]
        public void SetUp()
        {
            this.categoryRepoMock = new Mock<ICategoryRepository>();
            this.serviceUnderTest = new CategoryService(this.categoryRepoMock.Object);
        }

        [Test]
        public async void GetAllAsync_Test()
        {
            // arrange:
            var categories = new List<Category>();
            this.categoryRepoMock.Setup(x => x.GetAllAsync()).ReturnsAsync(categories);

            // act:
            var results = await this.serviceUnderTest.GetAllAsync();

            // assert:
            results.ShouldEqual(categories);
        }

    }
}
