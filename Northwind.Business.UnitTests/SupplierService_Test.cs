﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Northwind.CommonObject;
using Northwind.Persistance;
using Northwind.TestingUtilities;
using NUnit.Framework;

namespace Northwind.Business.UnitTests
{
    [TestFixture(Category = "SupplierService Tests")]
    public class SupplierService_Test
    {
        private Mock<ISupplierRepository> supplierRepoMock;
        private SupplierService serviceUnderTest;

        [SetUp]
        public void SetUp()
        {
            this.supplierRepoMock = new Mock<ISupplierRepository>();
            this.serviceUnderTest = new SupplierService(this.supplierRepoMock.Object);
        }

        [Test]
        public async void GetAllAsync_Test()
        {
            // arrange:
            var suppliers = new List<Supplier>();
            this.supplierRepoMock.Setup(x => x.GetAllAsync()).ReturnsAsync(suppliers);

            // act:
            var results = await this.serviceUnderTest.GetAllAsync();

            // assert:
            results.ShouldEqual(suppliers);
        }
    }
}
